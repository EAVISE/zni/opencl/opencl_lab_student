# Solutions for Session 3 shortcut_v1

There is a very simple solution: just swap the role of the indexes `row` and `col`:

```kernel
__kernel void shortcut_v1(__global float* res, __global const float* dis, int N) {
    // int row = get_global_id(0);
    // int col = get_global_id(1);
    int col = get_global_id(0); // faster
    int row = get_global_id(1);

    // Ensure the indices are within the bounds of the matrix
    // printf("%d %d\n", row, col);
    if (row < N && col < N) {
        float temp = 1.0e30;
        for (int k = 0; k < N; k++) {
            temp = min(temp,dis[row * N + k] + dis[k * N + col]);
        }
        res[row * N + col] = temp; // Write the computed value to the output matrix
    }
}

```

Now on the line `float x = d[n*rol+ k];` the memory addresses that the sub-group accesses are good; we are accessing only two distinct elements:

```c
d[0],    d[0],    ..., d[0],
d[1000], d[1000], ..., d[1000].
```

And on the line `float y = d[n*k + col];` the memory addresses that the sub-group accesses are also good; we are accessing one continuous part of the memory:

```c
d[0], d[1], ..., d[15],
d[0], d[1], ..., d[15].
```

![Lesson3_5](img\Lesson3_5.gif)

The above animation illustrates what happens in the first block of threads, for the first 6 iterations of the loop:

- *Orange:* which elements are accessed by the first *thread*.
- *Blue:* which elements are accessed by the first *warp of threads*.
- *Black:* which elements are accessed by the first *block of threads* (assuming that they are executing in a synchronized manner, which is not necessarily the case).

The key observation is that the blue dots are either continuous memory addresses or they only span two rows, which is good.
