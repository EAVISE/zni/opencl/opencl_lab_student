/*
 * @Date: 2024-03-27 10:35:43
 * @Author: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-04-10 22:15:59
 * @FilePath: /Lab_OpenCL/student/Lesson3/shortcut_cpu/main.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "time_utils.h" // Custom utility for timing operations


#define INFINITY_FLOAT 1.0e30

float min(float a, float b) {
    return a < b ? a : b;
}

/**
 * @param res pointer to the result matrix
 * @param dis pointer to the distance matrix
 * @param n dimension of the input matrix
 */
void step(float *res, const float *dis, int n) {
    // TODO
}

int main(int argc, char *argv[]) {
    if (argc > 2) {
        printf("Usage: %s <dimension>\n", argv[0]);
        return 1;
    }else if (argc == 1){
        int n = 3;
        const float dis[] = {0, 8, 2, 
                            1, 0, 9, 
                            4, 5, 0};
        float res[n * n];
        step(res, dis, n);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                printf("%f ", res[i * n + j]);
            }
            printf("\n");
        }
        return 0;
    }

    int n = atoi(argv[1]);

    // Allocate memory for matrix dis and matrix res
    float *dis = (float *)malloc(n * n * sizeof(float));
    float *res = (float *)malloc(n * n * sizeof(float));

    // Generate random matrix dis
    srand(time(NULL));
    for (int row = 0; row < n; ++row) {
        for (int col = 0; col < n; ++col) {
            if (row == col) {
                dis[row * n + col] = 0; // Set diagonal elements to 0
            } else {
                dis[row * n + col] = rand() % 19 + 1; // generating random numbers between 1 and 20
                // dis[row * n + col] = row * n + col;
            }
        }
    }

    // Compute shortest paths
    time_measure_start("computation");
    step(res, dis, n);
    time_measure_stop_and_print("computation");

    // Free dynamically allocated memory
    free(dis);
    free(res);

    return 0;
}