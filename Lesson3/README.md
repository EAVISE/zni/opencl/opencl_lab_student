# Lesson 3

## 0. The shortcut problem

We will use the following simple graph problem as our example throughout this lesson. We have a directed graph with $n$ nodes. The nodes of the graph are labeled with numbers $0,1, \ldots, n-1$. There is a directed edge between each pair of the nodes; the cost of the edge between nodes $i$ and $j$ is $d_{i j}$; we will assume that $d_{i j}$ is a nonnegative real number. For convenience, we write $d_{i i}=0$ for each node $i$.

However, the costs do not necessarily satisfy the triangle inequality. We might have an edge of cost $d_{i j}=10$ from node $i$ to $j$, but there might be an intermediate node $k$ with $d_{i k}=2$ and $d_{k j}=3$. Then we can follow the route $i \rightarrow k \rightarrow j$ at a total cost of $2+3=5$, while the direct route $i \rightarrow j$ would cost 10 .

Our task is to find for all $i$ and $j$ what is the cost of getting from $i$ to $j$ by taking **at most two edges**. If we write $r_{i j}$ for the result, then clearly

$$ r_{ij}=\min_{k} \left(d_{i k}+d_{k j}\right) $$

where $k$ ranges over $0,1, \ldots, n-1$. Note that we will also consider here e.g. the route $i \rightarrow i \rightarrow j$, and hence we will also find a path of one edge if it happens to be cheapest.

So for example, if $n=3$, this graph

![Lesson3_1](img/Lesson3_1.png)

can be represented by

![Dis](img/dis.png)

Then we calculate the minimum distance from 0 to 1. When 2 is used as an intermediate node, the distance is 7, which is less than 8, so the minimum distance from 0 to 1 is updated to 7. This process is shown as

![Lesson3_2](img/Lesson3_2.png)

After completing all calculations, the final shortest distance between any two points should be represented as the following matrix and the graph

![Res](img/res.png)

![Lesson3_3](img/Lesson3_3.png)

## 1. CPU Version

We will implement a function `step` with the following prototype:

```c
void step(float* res, const float* dis, int n);
```

Here `n` denotes the number of nodes, `dis` contains the input, and `res` will contain the result. Both `dis` and `res` are pointers to arrays with `n * n` floats.

The cost of the edge from node `i` to node `j` is stored in `dis[n*i + j]`. Similarly, the cost of getting from `i` to `j` by following at most two edges will be stored in `res[n*i + j]`. Here `0 <= i < n` and `0 <= j < n`.

### **Tasks:**

1. **Implement `step` Function**.

### **Instructions:**

- Compile and run your program using `./shortcut_cpu`, check if your result is correct.
- Run your program while passing the dimension of matrix as an argument. For example: `./shortcut_cpu 3000` .

## 2. GPU Version 0

We will now look at how to implement the solution using OpenCL.

### **Tasks:**

1. **Write the kernel**.
2. **Complete the `main` code.**
   - Allocate necessary buffers on the device to store the distance matrix and the result matrix. Use OpenCL memory management functions to handle the data transfer between the host and the device.
   - Set the appropriate arguments for your kernel, ensuring that you've correctly passed the distance matrix and prepared for receiving the result matrix.
   - Execute the kernel with a work size corresponding to the dimensions of your distance matrix.
   - Clean up your allocated resources both on the host and the device to prevent memory leaks.

### **Instructions:**

- Compile and run your program using `./shortcut_v0`, check if your result is correct.
- Run your program while passing the dimension of matrix as an argument. For example: `./shortcut_v0 3000`. Compare the running speed of current version with the CPU version.

## 3. GPU Version 1: Better memory access pattern

The main reason for the poor performance of the previous version is a poor memory access pattern. To fix it, we need to understand a little bit of the GPU hardware.

A very good rule of thumb for modern computers is that **reading memory in a linear order is a good idea**. For example, instead of reading elements `d[0]`, `d[n]`, `d[2*n]`, etc., it would be much better to read `d[0]`, `d[1]`, `d[2]`, etc., in this order.

There are two main reasons for favoring linear reading:

- Memory is always transmitted in full **cache lines** between the main memory and various levels of cache in the GPU. A cache line of GPU is usually 128 bytes. Hence, when you read `d[0]` (which takes only 4 bytes), you are simultaneously transmitting also e.g. `d[1]`, `d[2]`, …, `d[15]` from the main memory to the caches near the GPU. This would of course be convenient, if these were exactly the elements that your code would need next.
- Linear reading is **what the hardware expects** and what it is optimized for. When the GPU notices that a program seems to be reading consecutive elements of memory, [hardware prefetching](https://en.wikipedia.org/wiki/Cache_prefetching) will automatically kick in: the GPU will guess that the program will keep requesting consecutive words of memory, and it will start to fetch data from the main memory to the cache memory already before the program has asked for it.

In OpenCL, the GPU operates in groups known as **work-groups**, which can vary in size but often align with the device's warp size for efficiency. For instance, although the size of a work-group is set to 16x16, i.e. a total of 256 threads, in practice hardware such as GPUs usually operate in smaller execution units (e.g. groups of 32 threads). These smaller execution units are often called "sub-groups" (similar to warps in CUDA), and the threads within these units are executed synchronously. This division may arrange the threads into a two-dimensional grid, with each division representing two rows of a 16x16 matrix. For example, the first warp corresponds to threads with these (x, y) indexes:

```c
(0,0), (1,0), ... (15,0), 
(0,1), (1,1), ... (15,1).
```

Since all threads of a  work-group operate in a fully synchronized fashion, if one thread is reading `d[n*i + k]`, all threads of the warp are reading it simultaneously.

Let us look at the line `float x = d[n*row+ k];`. To have nice round numbers, let us assume that n = 1000, and let us focus on the **first sub-group of the first block**; all other sub-groups are similar. To set the value of `x`, the 32 threads in the sub-group will try to read the following array elements simultaneously:

```c
d[0], d[1000], ..., d[15000],
d[0], d[1000], ..., d[15000].
```

This is **bad** from the perspective of the memory controller. The memory reads are likely to span 16 different cache lines, and we are only using 1 element per cache line.

Incidentally, the memory access pattern is much better for the line `float y = d[n*k + col];`:

```c
d[0], d[0], ..., d[0],
d[1], d[1], ..., d[1].
```

We are just accessing 2 different elements, both likely to be on the same cache line, so the memory system will need to do much less work here. The hardware is clever enough to realize that we are reading the same element many times.

Note that the solution with a transpose does not help with the problematic memory access pattern at all.

![Lesson3_4](img/Lesson3_4.gif)

The above animation illustrates what happens in the first block of threads, for the first 6 iterations of the loop:

- *Orange:* which elements are accessed by the first *thread*.
- *Blue:* which elements are accessed by the first *sub-group of threads*.
- *Black:* which elements are accessed by the first *work-group of threads* (assuming that they are executing in a synchronized manner, which is not necessarily the case).

The key observation is that the blue dots often span very many rows, which is bad.

### Tasks:

1. **Write the kernel**.
2. **Complete the `main` code.**

### **Instructions:**

- Compile and run your program using `./shortcut_v1`, check if your result is correct.
- Run your program while passing the dimension of matrix as an argument. For example: `./shortcut_v1 3000` . Compare the running speed of current version with the previous versions.

### **Question:**

- **Would it help if we transpose the matrix? Why?**

## 4. GPU Version 2: Data reuse in registers

What would be the next steps for improving the performance?

The main bottleneck in our current implementation is not in parallelism but in the memory accesses. In **version 0** we had lots of memory accesses, most of them bad from the perspective of the memory system. In **version 1** we fixed the memory access pattern so that different threads from the same warp read adjacent memory locations, but we are still doing equally many memory accesses.

To further improve the performance we will need to find a way to do fewer memory accesses. **The approach is to heavily reuse data in registers.**

We will use the idea of **matrix tiling**. The computation of the matrix will be divided into smaller, manageable blocks or 'tiles'. Recall the different hardware structures for GPUs to store data, if we can keep the data needed for computation close to the processor, we will reduce the time spent fetching data from memory. By loading small segments of the matrix into local memory or registers, the program reduces the number of global memory accesses and takes advantage of the fast access speed of local memory. Each workgroup handles a tile, performing computations that require data from that specific section of the matrix. After computing local results, they are written back to the global memory.

### **Tasks:**

1. **Write the kernel**.
   - Understand the meaning of `ia`, `ib`, `ic` and `ja`, `jb`, `jc`.
   - Calculate the right index `i` and `j`.
   - Finish all the TODOs.
2. **Complete the `main` code.**
   - Similar to Version 0.
   - Think about the transpose of `dis`.
   - What to do if we want to run multiple different kernel?

### **Instructions:**

- Compile and run your program using `./shortcut_v2`, check if your result is correct.
- Run your program while passing the dimension of matrix as an argument. For example: `./shortcut_v2 3000` or `./shortcut_v2 10000`. Compare the running speed of current version with the previous versions.

## 5. *GPU Version 3: Data reuse in shared memory

Until now, we have not discussed cooperation between threads. For the bravest and most curious of you, you can now look at **collaboration and coordination between threads in a block**. You can reference to this [tutorial](https://ppc.cs.aalto.fi/ch4/v3/). Notice that this tutorial is based on Cuda. For OpenCL, there is some difference (e.g. no notion of "warps"), but the main idea remains the same.

## Acknowledgments

This course is based on the [course of Jukka Suomela from the Aalto university](https://ppc.cs.aalto.fi/), which is available under CC BY 4.0 license.

This course is also under CC BY 4.0.
