/*
 * @Date: 2024-03-27 10:35:43
 * @Author: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-04-18 10:45:16
 * @FilePath: /OpenCL_Lab/student/Lesson3/shortcut_v0/main.c
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "time_utils.h" // Custom utility for timing operations
#include "ocl_utils.h"  // Custom utility for simplifying OpenCL operations

size_t global_work_size[2] = {16, 16}; // Size of the work group

void usage(const char *prog_name)
{
    printf("Usage:\n\t");
    printf("%s <num_samples>\n", prog_name);
}

int main(int argc, char *argv[])
{
    int n;
    float *dis = NULL;
    if (argc > 2) {
        printf("Usage: %s <dimension>\n", argv[0]);
        return 1;
    }else if (argc == 1){
        n = 3;
        static const float preset_dis[] = {0, 8, 2, 
                                            1, 0, 9, 
                                            4, 5, 0};
        dis = malloc(n * n * sizeof(float));
        memcpy(dis, preset_dis, sizeof(preset_dis));
    }else{
        n = atoi(argv[1]);
        dis = malloc(n * n * sizeof(float));
        // Generate random matrix dis
        srand(time(NULL));
        for (int row = 0; row < n; ++row) {
            for (int col = 0; col < n; ++col) {
                if (row == col) {
                    dis[row * n + col] = 0; // Set diagonal elements to 0
                } else {
                    dis[row * n + col] = rand() % 19 + 1; // generating random numbers between 1 and 20
                    // dis[row * n + col] = row * n + col;
                }
            }
        }
    }
    float *res = (float *)malloc(n * n * sizeof(float));
    global_work_size[0] = n; // ! What is the impact of global_work_size?
    global_work_size[1] = n;
    cl_int error;

    // Step 1: Get the available platforms
    cl_platform_id platform = ocl_select_platform();
    // Step 2: Get the available devices
    cl_device_id device = ocl_select_device(platform);
    // Step 3+4: Create the context and the command queue
    ocl_init(device);
    // Step 5+6+7: Load the kernel from file, create and build program
    ocl_create_program("kernel.cl", ""); 

    // TODO Create a buffer on the device and copy data from host to device


    // Create the kernel
    cl_kernel kernel = clCreateKernel(g_program, "shortcut_v0", &error);
    ocl_err(error);

    time_measure_start("total"); // Start timing
    // TODO Set kernel arguments


    // Launch the kernel
    time_measure_start("computation");
    ocl_err(clEnqueueNDRangeKernel(g_command_queue, kernel, 2, NULL,
                               global_work_size, NULL, 0, NULL, NULL));
    ocl_err(clFinish(g_command_queue)); // Wait for the kernel to complete
    time_measure_stop_and_print("computation");

    // TODO Read back the result from the device to the host


    time_measure_stop_and_print("total"); // Stop timing

    // Result output
    // Only show 3x3 result for brevity
    printf("Your output is:\n");
    for (int row = 0; row < 3; ++row) {
        for (int col = 0; col < 3; ++col) {
            printf("%f ", res[row * n + col]);
        }
        printf("\n");
    }
    if (argc == 1){
        printf("Expected output is:\n0 7 2\n1 0 3\n4 5 0\n");
    }

    // TODO Free memory
    free(dis);
    free(res);
    clReleaseKernel(kernel);
    ocl_cleanup();

    return 0;
}
