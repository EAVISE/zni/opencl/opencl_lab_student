# Parallel Computing Using OpenCL

2024 Spring

## Welcome to OpenCL Lab Sessions

As you embark on this OpenCL lab series, you're about to unlock the extraordinary capabilities of parallel computing using GPU. OpenCL provides the tools to maximize computational efficiency across CPUs and GPUs, and through these sessions, you'll dive deep into its rich ecosystem.

![OpenCL](opencl.jpg)

**Session 1: Getting Started with OpenCL**

Kick things off with the basics. You'll first ensure your development environment is ready with a "**Hello World**" exercise. Then, step by step, you'll play with OpenCL **threads**, learning to write functions that lay the foundation for more advanced operations. By tackling "**addvector**" and "**muxvector**" exercises, you'll get your first taste of writing and executing kernels.

**Session 2: Calculating Pi**

The quest for π introduces you to the power of OpenCL for calculating vectors and solving mathematical problems. Start with a **traditional approach** to calculate Pi, then shift gears to translate this logic into an **OpenCL kernel**. Running this on a GPU, you'll see firsthand the benefits of parallel computation on GPU.

**Session 3: Enhancing Memory Management**

Efficient memory management is crucial for optimizing performance in OpenCL. This session evolves through three stages, each focusing on different strategies:

- **Version 1: Optimizing memory access patterns**
- **Version 2: Data reuse in registers**
- **Version 3: Data reuse in shared memory** (*if time allows)

You'll tackle the "shortcut problem". This real-world problem involving a large number of loop calculations is a perfect test bed for applying and witnessing the efficiency gains.

**Sessions 4-6: The N-Body Project**

The grand finale of this lab series is the n-body simulation project. This project will require you to apply everything you've learned to simulate the gravitational interactions between multiple bodies in space.

## Evaluation

Evaluation for this course is based on **class participation (20%)** and the result of **N-body problems (80%)**. You will be required to submit your code and a short report at the end of the course. The score will be based on the optimizations you attempted to apply and how well they worked. You may receive **extra points (10%)** if you propose important modifications or contributions to the code for this course, or if your code achieves such significant optimization results that it may be used as a model.
