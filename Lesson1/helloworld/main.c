/*
 * @Date: 2024-03-20 16:21:48
 * @Author: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-04-09 18:27:45
 * @FilePath: /Lab_OpenCL/student/helloworld/main.c
 */
#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("Hello, World from host!\n");
    return 0;
}
