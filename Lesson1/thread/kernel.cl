// kernel.cl
__kernel void detailed_id_kernel(
    __global int* globalIds,
    __global int* localIds,
    __global int* groupIds,
    __global int* numGroups) {

    int gid = get_global_id(0);
    int lid = get_local_id(0);
    int grpid = get_group_id(0);
    int ngrps = get_num_groups(0);

    globalIds[gid] = gid;
    localIds[gid] = lid;
    groupIds[gid] = grpid;
    numGroups[gid] = ngrps;
}
