/*
 * @Date: 2024-03-20 16:30:47
 * @Author: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-04-09 18:37:05
 * @FilePath: /Lab_OpenCL/student/thread/main.c
 */
#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_ELEMENTS 1024

int main() {
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;
    cl_mem bufferGlobalIds, bufferLocalIds, bufferGroupIds, bufferNumGroups;
    cl_int err;

    size_t global_work_size = NUM_ELEMENTS;
    int *globalIds = (int*)malloc(sizeof(int) * NUM_ELEMENTS);
    int *localIds = (int*)malloc(sizeof(int) * NUM_ELEMENTS);
    int *groupIds = (int*)malloc(sizeof(int) * NUM_ELEMENTS);
    int *numGroups = (int*)malloc(sizeof(int) * NUM_ELEMENTS);

    // TODO Step 1: Get the first available platform
    err = 
    // TODO Step 2: Get the first available device
    err = 
    // TODO Step 3: Create the context
    context = 
    // TODO Step 4: Create the command queue
    const cl_command_queue_properties properties[] = {CL_QUEUE_PROPERTIES, 0, 0};
    queue = 

    // Step 5: Load the kernel source code
    //*/ OPTION 1: From string
    char* kernelSource = "__kernel void detailed_id_kernel("
                                "__global int* globalIds,"
                                "__global int* localIds,"
                                "__global int* groupIds,"
                                "__global int* numGroups) {"

                                "int gid = get_global_id(0);"
                                "int lid = get_local_id(0);"
                                "int grpid = get_group_id(0);"
                                "int ngrps = get_num_groups(0);"

                                "globalIds[gid] = gid;"
                                "localIds[gid] = lid;"
                                "groupIds[gid] = grpid;"
                                "numGroups[gid] = ngrps;"
                                "}";

    //*/

    /*/ OPTION 2: From a file
    FILE* file = fopen("kernel.cl", "r");
    if (!file) {
        fprintf(stderr, "Failed to load the kernel.\n");
        return EXIT_FAILURE;
    }
    fseek(file, 0, SEEK_END);
    size_t source_size = ftell(file);
    rewind(file);

    char* kernelSource = (char*)malloc(source_size + 1);
    kernelSource[source_size] = '\0';
    fread(kernelSource, sizeof(char), source_size, file);
    fclose(file);
    //*/

    // TODO Step 6: Create the program
    program = 
    // TODO Step 7: Build the program
    err = 

    if (err != CL_SUCCESS) {
        // If there's a build error, retrieve and print log
        size_t logSize;
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
        char* buildLog = (char*)malloc(logSize);
        if (buildLog) {
            clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, logSize, buildLog, NULL);
            buildLog[logSize-1] = '\0';
            printf("Error in kernel build:\n%s\n", buildLog);
            free(buildLog);
        }
    }
    
    // TODO Step 8: Create the kernel
    kernel = 
    if (err != CL_SUCCESS) {
        printf("Failed to create kernel. Error: %d\n", err);
    }

    // TODO Step 9: Create the buffers
    bufferGlobalIds = 
    bufferLocalIds = 
    bufferGroupIds = 
    bufferNumGroups = 

    // TODO Step 10: Set the kernel arguments
    err = 

    // TODO Step 11: Execute the kernel
    err = 
    if (err != CL_SUCCESS) {
        fprintf(stderr, "Failed to enqueue the kernel. Error: %d\n", err);
    }
    
    // Wait for the commands in the queue to finish:
    clFinish(queue);

    // TODO Step 12: Read the results
    err = 

    // Check the error on reading buffers
    if (err != CL_SUCCESS) {
        fprintf(stderr, "Failed to read the buffer. Error: %d\n", err);
    }
    
    // Print the results
    for (int i = 0; i < NUM_ELEMENTS; i++) {
        printf("Work-item %d Global ID: %d, Local ID: %d, Group ID: %d, Total Groups: %d\n",
               i, globalIds[i], localIds[i], groupIds[i], numGroups[i]);
    }

    // Cleanup
    clReleaseMemObject...
    clRelease...
    free...
    if (kernelSource != NULL) {
        free(kernelSource);
    }
    return 0;
}
