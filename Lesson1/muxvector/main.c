#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>

const int N = 10; // Defining the dimension of the square matrix

int main() {
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;
    cl_mem bufferA, bufferB, bufferC;
    cl_int err;

    // Define the size for the square matrices
    size_t matrix_size = N * N;
    float *A = (float*)malloc(sizeof(float) * matrix_size);
    float *B = (float*)malloc(sizeof(float) * matrix_size);
    float *C = (float*)malloc(sizeof(float) * matrix_size);

     // Initialize matrices A and B with some values
    for(size_t i = 0; i < matrix_size; i++) {
        A[i] = (float)i; 
        B[i] = (float)i; 
        // A[i] = 1; // Simplified example value
        // B[i] = 1; // Simplified example value
    }


    // TODO Step 1: Get the first available platform
    err = 
    // TODO Step 2: Get the first available device
    err = 
    // TODO Step 3: Create the context
    context = 
    // TODO Step 4: Create the command queue
    const cl_command_queue_properties properties[] = {CL_QUEUE_PROPERTIES, 0, 0};
    queue = 

    // TODO Step 5: Load the kernel source code
    /*/ OPTION 1: From string
    char* kernelSource = "";

    //*/

    //*/ OPTION 2: From a file
    FILE* file = fopen("kernel.cl", "r");
    if (!file) {
        fprintf(stderr, "Failed to load the kernel.\n");
        return EXIT_FAILURE;
    }
    fseek(file, 0, SEEK_END);
    size_t source_size = ftell(file);
    rewind(file);

    char* kernelSource = (char*)malloc(source_size + 1);
    kernelSource[source_size] = '\0';
    fread(kernelSource, sizeof(char), source_size, file);
    fclose(file);
    //*/

    // TODO Step 6: Create the program
    program = 
    // TODO Step 7: Build the program
    err = 

    if (err != CL_SUCCESS) {
        // If there's a build error, retrieve and print log
        size_t logSize;
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
        char* buildLog = (char*)malloc(logSize);
        if (buildLog) {
            clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, logSize, buildLog, NULL);
            buildLog[logSize-1] = '\0';
            printf("Error in kernel build:\n%s\n", buildLog);
            free(buildLog);
        }
    }
    
    // TODO Step 8: Create the kernel
    kernel = 
    if (err != CL_SUCCESS) {
        printf("Failed to create kernel. Error: %d\n", err);
    }

    // TODO Step 9: Create the buffers
    

    // TODO Step 10: Set the kernel arguments
    err = 

    // ! TODO Copy data from host to device
    err = 

    // TODO Step 11: Execute the kernel
    size_t global_wor...; // TODO 2D global work size.
    err = 
     if (err != CL_SUCCESS) {
        fprintf(stderr, "Failed to enqueue the kernel. Error: %d\n", err);
    }
    
    // Wait for the commands in the queue to finish:
    clFinish(queue);

    // TODO Step 12: Read the results
    err = 


    // Check the error on reading buffers
    if (err != CL_SUCCESS) {
        fprintf(stderr, "Failed to read the buffer. Error: %d\n", err);
    }
    
    // Print the results
    printf("Result matrix C:\n");
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            printf("%6.2f ", C[i * N + j]);
        }
        printf("\n");
    }

    // TODO Cleanup
    
    return 0;
}
