# Lesson 1

## 1. helloworld

You have a normal C code of helloworld. To enable OpenCL, add these code:

```C
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
```

Now type `make` in the command line, check if there are any issues.

### **Questions:**

1. Go through the `Makefile`, how is the OpenCL library added to the compiler?
2. What is the version of your OpenCL?

## 2. thread

In this exercise, you will get to know how OpenCL organizes its execution model through work-items, work-groups, and various identifiers. You'll start by completing a template program that captures and prints the global, local, group IDs, and the total number of groups for each work-item in a parallel execution.

### **Tasks:**

1. **Platform and Device Setup**: Identify and select the first available OpenCL platform and device.
2. **Context and Command Queue**: Create an OpenCL context for the selected device, and then a command queue.
3. **Kernel Creation**: You have two options for loading your kernel source code—directly from a string within your program, or by reading from an external `.cl` file. Choose one method by commenting the other one.
4. **Program and Kernel Building**: Compile your kernel code into an OpenCL program, and then create a kernel object from this program.
5. **Buffer Management**: Initialize memory buffers to hold your kernel's input and output data. This involves creating buffers and setting kernel arguments.
6. **Execution and Retrieval**: Execute your kernel and retrieve the results.

You can refer to "opencl30-reference-guide.pdf" to find useful functions, and google them in case of needed. You will find the explanation in details on "Khronos Registry".

### **Instructions:**

- Complete the code by filling in the TODO sections. These tasks guide you through setting up your OpenCL environment, creating contexts, and executing kernels, etc..
- Type `make` in the command line to compile your program. 
- Run your program and observe the output. Analyze how OpenCL's execution model organizes work-items into groups and assigns identifiers.

### **Questions:**

1. How do Global and Local IDs relate to each other?
2. Why are there IDs and Groups?
3. Why are there an upper limit for Local ID?

## 3. addvector

In this exercise, we will implement a basic **vector addition kernel**. You'll work through the entire process from setting up your OpenCL environment, creating buffers, and running your kernel to performing clean-up operations as in `thread`. But this time, you have to write the kernel, and transfer data between host and device.

### **Tasks:**

Complete the provided template with TODOs to implement vector addition in OpenCL. Your code from "thread" will be helpful. You will also need to write the kernel.

### **Questions:**

1. **Comparison with Exercise 2**: What steps in this exercise are the same as in Exercise 2, and what steps are new? 
2. **How many allocations of memory space are to be made? How many transfers of data? When? Where?**

## 4. muxvector

**Matrix multiplication** is a fundamental operation in numerous scientific and engineering applications. The goal of this exercise is to implement the multiplication of two square matrices. We will try to work with two-dimensional data in a parallel environment.

### **Tasks:**

Complete the provided template with TODOs to implement vector multiplication in OpenCL. Your code from "thread" will be helpful. You will also need to write the kernel.

### **Questions:**

1. **How does the work size in this exercise compare to that in the vector addition exercise? Why?**
2. **Optimizing Matrix Multiplication**: Based on your implementation, what strategies could further optimize the performance of matrix multiplication in OpenCL? We will discover in Lesson 3:)
