/*
 * @Date: 2024-03-20 13:54:33
 * @LastEditors: Zijie Ning zijie.ning@kuleuven.be
 * @LastEditTime: 2024-04-10 17:16:19
 * @FilePath: /Lab_OpenCL/student/pi/main.c
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "time_utils.h"


void usage(const char *prog_name)
{
    printf("Usage:\n\t");
    printf("%s <num_samples>\n", prog_name);
}

cl_float2 *create_vectors(int num_vectors)
{
    cl_float2 * vectors = (cl_float2 *)malloc(sizeof(cl_float2) * num_vectors);

    for (int i = 0; i < num_vectors; ++i)
    {
        vectors[i].s[0] = (float)rand() / (float)RAND_MAX;
        vectors[i].s[1] = (float)rand() / (float)RAND_MAX;
    }
    return vectors;
}

/**
 * @brief Generates a random 2D vector with coordinates between 0 and 1.
 *
 * This function generates a random 2D vector with coordinates between 0 and 1.
 * It is used in the calculation of Pi using Monte Carlo method.
 *
 * @return A cl_float2 structure containing the random 2D vector.
 */
cl_float2 create_vector(void)
{
    cl_float2 vector;
    vector.s[0] = (float)rand() / (float)RAND_MAX;
    vector.s[1] = (float)rand() / (float)RAND_MAX;

    return vector;
}

/**
 * @brief Checks if a 2D vector is inside the unit circle.
 *
 * This function checks if a given 2D vector is inside the unit circle,
 * which is a circle with a radius of 1 centered at the origin (0, 0).
 *
 * @param vector The 2D vector to be checked.
 *
 * @return True if the vector is inside the unit circle, false otherwise.
 */
bool is_in_unit_circle(cl_float2 vector)
{
    // TODO
}

/**
 * @brief Calculates the value of Pi using the Monte Carlo method.
 *
 * This function calculates the value of Pi using the Monte Carlo method.
 * It generates a specified number of random 2D vectors and checks if they are inside the unit circle.
 * The ratio of vectors inside the unit circle to the total number of vectors is then used to estimate the value of Pi.
 *
 * @param vectors A pointer to an array of cl_float2 structures containing the random 2D vectors.
 * @param num_samples The number of random 2D vectors to be generated and checked.
 *
 * @return The estimated value of Pi.
 */
float calc_pi(cl_float2 *vectors, int num_samples)
{
    int num_in_unit_circle = 0;
    
    // TODO
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        usage(argv[0]);
        exit(-1);
    }
    int num_samples = atoi(argv[1]);
    if (num_samples <= 0)
    {
        usage(argv[0]);
        exit(-1);
    }
    srand(time(NULL));

    time_measure_start("total");
    time_measure_start("create vectors");
    cl_float2 * vectors = create_vectors(num_samples);
    time_measure_stop_and_print("create vectors");
    time_measure_start("calculation");
    float pi = calc_pi(NULL, num_samples);
    time_measure_stop_and_print("calculation");
    free(vectors);
    time_measure_stop_and_print("total");

    printf("Calculated PI using %d samples, result is %f\n", num_samples, pi);
}
