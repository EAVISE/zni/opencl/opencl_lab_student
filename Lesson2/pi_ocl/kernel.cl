// Inline function to determine if a given point is inside the unit circle.
// Returns 1 if inside the circle, 0 otherwise.
inline int is_in_unit_circle(float2 vector)
{
    // TODO
}

// OpenCL kernel function to calculate the number of points that lie within the unit circle,
// which is used to estimate the value of pi.
__kernel void calc_pi(__global float2 *vectors, // Input: Array of points
                      int size,                  // Input: Number of points
                      __global int *result,      // Output: Array to store the result per work-group
                      __local int *scratch)      // Local memory scratch space for reduction
{
    const int gid = get_global_id(0); // Global ID of the current work-item
    const int lid = get_local_id(0);  // Local ID within the current work-group
    const int global_size = get_global_size(0); // Total number of work-items
    const int local_size = get_local_size(0);   // Number of work-items in the work-group
    const int group_id = get_group_id(0);       // Work-group ID

    int vectors_per_thread = size / global_size; // Divide work evenly among work-items

    scratch[lid] = 0; // Initialize local memory
    // Each work-item processes its subset of points and accumulates the count of points inside the unit circle
    // TODO 

    barrier(CLK_LOCAL_MEM_FENCE); // Ensure all work-items have completed their portion

    // ? QUESTION What are we doing from here to the end?
    int skip = 1;
    for (int i = local_size; i >= 1; i /= 2)
    {
        if (lid % (skip * 2) == 0)
        {
            scratch[lid] = scratch[lid] + scratch[lid + skip];
        }
        skip *= 2;
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (lid == 0)
    {
        result[group_id] = scratch[0];
    }
}
