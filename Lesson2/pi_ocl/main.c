#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "time_utils.h" // Custom utility for timing operations
#include "ocl_utils.h"  // Custom utility for simplifying OpenCL operations

// size_t vector_size = 1024 * 1024 * 4; // Size of the vector array
size_t vector_size;
const size_t WORK_GROUP_SIZE = 256; // Size of the work group

void usage(const char *prog_name)
{
    printf("Usage:\n\t");
    printf("%s <num_samples>\n", prog_name);
}

// Create and initialize a vector on the host and then transfer it to the device
cl_mem create_and_init_vector(void)
{
    cl_int error;
    cl_float2 *host_vec = malloc(sizeof(cl_float2) * vector_size);
    for (int i = 0; i < vector_size; ++i)
    {
        host_vec[i].s[0] = (float)rand() / (float)RAND_MAX;
        host_vec[i].s[1] = (float)rand() / (float)RAND_MAX;
    }

    // Create a buffer on the device and copy data from host to device
    cl_mem dev_vec = clCreateBuffer(g_context,
            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
            sizeof(cl_float2) * vector_size, host_vec, &error);
    ocl_err(error);
    ocl_err(clFinish(g_command_queue)); // Ensure the command is finished
    free(host_vec); // Free the host memory
    return dev_vec;
}

// Create a buffer on the device for storing results
cl_mem create_result_buffer(void)
{
    cl_int error;
    cl_mem dev_vec = clCreateBuffer(g_context, CL_MEM_WRITE_ONLY,
            sizeof(float) * vector_size, NULL, &error);
    ocl_err(error);
    return dev_vec;
}

// Main computation to calculate pi
double calc_pi(void)
{
    cl_int error;
    // Create device buffers
    cl_mem dev_vectors = create_and_init_vector();
    cl_mem dev_result = create_result_buffer();
    size_t global_work_sizes[] = { vector_size / 64 };
    const int result_size = (global_work_sizes[0] / WORK_GROUP_SIZE);
    cl_int *host_result = malloc(sizeof(cl_int) * result_size);

    // Create the kernel
    cl_kernel kernel = clCreateKernel(g_program, "calc_pi", &error);
    ocl_err(error);

    // Set kernel arguments
    int arg_num = 0;
    ocl_err(clSetKernelArg(kernel, arg_num, sizeof(cl_mem), &dev_vectors));
    arg_num++;
    ocl_err(clSetKernelArg(kernel, arg_num, sizeof(cl_int), &vector_size));
    arg_num++;
    ocl_err(clSetKernelArg(kernel, arg_num, sizeof(cl_mem), &dev_result));
    arg_num++;
    ocl_err(clSetKernelArg(kernel, arg_num, WORK_GROUP_SIZE * sizeof(int), NULL)); // Local memory size
    arg_num++;

    // Launch the kernel
    time_measure_start("computation");
    ocl_err(clEnqueueNDRangeKernel(g_command_queue, kernel, 1, NULL,
                                   global_work_sizes, &WORK_GROUP_SIZE, 0, NULL, NULL));
    ocl_err(clFinish(g_command_queue)); // Wait for the kernel to complete
    time_measure_stop_and_print("computation");

    // Read back the result from the device to the host
    ocl_err(clEnqueueReadBuffer(g_command_queue, dev_result, CL_TRUE,
                                0, sizeof(cl_int) * result_size, host_result, 0, NULL, NULL));

    // Accumulate the results to calculate pi
    int accumulator = 0;
    for (int i = 0; i < result_size; ++i)
        accumulator += host_result[i];

    clReleaseMemObject(dev_vectors); // Release
    clReleaseMemObject(dev_result);
    clReleaseKernel(kernel);

    return (double)accumulator / (double)vector_size * 4.0; // Calculate pi
}

int main(int argc, char *argv[])
{
    srand(time(NULL)); // Seed the random number generator

    // TODO Step 1: Get the available platforms
    cl_platform_id platform = 
    // TODO Step 2: Get the available devices
    cl_device_id device = 

    size_t max_mem_alloc_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(max_mem_alloc_size), &max_mem_alloc_size, NULL);
    printf("Maximum memory allocation size for device: %zu bytes\n", max_mem_alloc_size);
    size_t max_work_group_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_work_group_size), &max_work_group_size, NULL);
    printf("Maximum work group size for device: %zu\n", max_work_group_size);
    
    // TODO Step 3+4: Create the context and the command queue
    
    // TODO Step 5+6+7: Load the kernel from file, create and build program
    

    if (argc != 2)
    {
        usage(argv[0]);
        exit(-1);
    }
    size_t input_vector_size = (size_t)atoi(argv[1]);
    if (input_vector_size <= 0 || input_vector_size * sizeof(cl_float2) > max_mem_alloc_size)
    {
        printf("Invalid vector size. Please ensure it is a positive number and does not exceed the device's memory limits.\n");
        usage(argv[0]);
        exit(-1);
    }
    // Adjust vector_size/64 to make sure it is a multiple of WORK_GROUP_SIZE
    size_t remainder = input_vector_size % (64 * WORK_GROUP_SIZE);
    if (remainder != 0) {
        vector_size = input_vector_size + (64 * WORK_GROUP_SIZE - remainder);
    } else {
        vector_size = input_vector_size;
    }
    printf("Adjusted vector size to %zu to be a multiple of WORK_GROUP_SIZE (%zu).\n", vector_size, WORK_GROUP_SIZE);

    time_measure_start("total"); // Start timing
    double pi = calc_pi(); 
    time_measure_stop_and_print("total"); // Stop timing
    printf("Pi = %f\n", pi); // Print the result

    ocl_cleanup();

    return 0;
}
