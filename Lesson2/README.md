# Lesson 2

## Introduction

The Monte Carlo method is a statistical technique that allows for the solving of mathematical problems through the use of random sampling. One of the most classic applications of this method is the estimation of π.

To estimate Pi using the Monte Carlo method, you simulate random points within a square that circumscribes a quarter circle. The ratio of the number of points that fall inside the quarter circle to the total number of points inside the square is proportional to the area of the quarter circle. Since the area of the quarter circle is π/4 times the area of the square, multiplying this ratio by 4 gives an approximation of Pi.

This method's accuracy improves with the number of points used, demonstrating the power of statistical methods and random sampling in solving problems that might be challenging to address through direct deterministic algorithms.

## 1. pi

In this exercise, you'll implement a CPU-based program to estimate the value of Pi (π) using the Monte Carlo method. This exercise is designed to familiarize you with the Monte Carlo algorithm.

### **Tasks:**

1. **Complete the `is_in_unit_circle` Function**: Implement the logic to determine if a given point (vector) lies within a unit circle.
2. **Complete the `calc_pi` Function**: Utilize the `is_in_unit_circle` function to calculate the ratio of points inside the unit circle to the total number of points, and use this ratio to estimate Pi.

### **Instructions:**

- We offer you some helper functions. **Go to the "common" folder first and `make`.**
- Fill in the missing parts of the code.
- Compile and run your program, passing the number of samples as an argument. For example: `./calc_pi_cpu 10000000` to use 10 million samples.
- Observe how the number of samples affects both the accuracy of the Pi estimation and the computation time.

## 2. pi_ocl

This exercise transitions from CPU to GPU, employing the OpenCL framework to estimate Pi using the Monte Carlo method. By leveraging the parallel processing power of GPUs, you're expected to achieve a more efficient computation, especially with a large number of samples.

### **Tasks:**

1. **Setup OpenCL Environment and Prepare the Kernel**: To symplify the code and provide useful features, we offer you some utile functions in "ocl_tuils.c". Go have a look and use them to complete the steps to initialize OpenCL, including selecting a platform, choosing a device, creating a context, setting up a command queue and prepare the kernel.
2. **Implement `is_in_unit_circle` Function**: Complete the implementation of the `is_in_unit_circle` function to determine if points lie inside the unit circle.
3. **Complete the Kernel.**

### **Instructions:**

- Compile and run your program, passing the number of samples as an argument. For example: `./calc_pi 10000000` to use 10 million samples.
- Observe how the number of samples affects both the accuracy of the Pi estimation and the computation time.
- Compare this run to the run on the CPU.

### **Questions:**

1. **What is the last part of the code provided in the kernel doing?**
2. **Comparison of Work Sizes**: Reflect on the choice of work size for this GPU-based implementation in comparison to the CPU-based one. How does the GPU architecture influence the optimal choice of work size?
3. **What does the maximum memory of the hardware limit? When will we reach that limit?**
